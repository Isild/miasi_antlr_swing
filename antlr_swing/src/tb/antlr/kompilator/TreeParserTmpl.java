/**
 * 
 */
package tb.antlr.kompilator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	protected LocalSymbols locals = new LocalSymbols();
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}

	protected Integer enterScope() {
		Integer x = locals.enterScope();
//		System.out.print("Enter scope: "+ x);
		
		return x;
	}
	
	protected Integer leaveScope() {
		Integer x = locals.leaveScope();
//		System.out.print("Leave scope: "+ x);

		return x;
	}
	
	protected Integer initializeVariable(String name, Integer scope) {
		String variableName = name + scope.toString();
		if(scope == 1) {
			if(globals.hasSymbol(variableName)) {
				throw new RuntimeException("Variable " + variableName + " already exist in global scope.");
			} else {
				globals.newSymbol(variableName);
			}
		} else {
			if(locals.hasSymbol(variableName)) {
				throw new RuntimeException("Variable " + variableName + " already exist in local scope.");
			} else {
				locals.newSymbol(variableName);
			}
		}
		return scope;
	}
	
	protected Integer isExist(String name, Integer scope) {
		String variableName = name + scope.toString();
		Integer globalScope = scope;
		String variableNameGlobal = name;
		if(scope == 1) {
			if(!globals.hasSymbol(variableName)) {
				throw new RuntimeException("Variable " + variableName + " is not existing in global scope.");
			} else {
				return 1; //scope 1
			}
		} else {
			if(!locals.hasSymbol(variableName)) { 
				String parentName = variableNameGlobal+globalScope.toString();
				while(globalScope > 0) {
					parentName = variableNameGlobal+globalScope.toString();
					System.out.print(parentName+"\n");
					if(globals.hasSymbol(parentName)) {
						return globalScope;
					}
					if(locals.hasSymbol(parentName)) {
//						 sprawdzam czy nie istnieje zmienna w przestrzeni niżej, tak jak w podanym przypadku poniżej
//						  {
//						  		var x(x2)
//						  		{
//						  			x=2(x3)
//						  		}
//						  }
//						  znaleźć x2
						  
						return globalScope;
					}
					globalScope --;
				}//*/
				
				throw new RuntimeException("Variable " + variableName + " is not existing in local scope.");
			} 
		}
		return 0;
	}
}

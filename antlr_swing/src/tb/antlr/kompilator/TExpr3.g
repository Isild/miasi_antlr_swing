tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer scope = 1;
  Integer sc = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d}) 
;

zakres : ^(BEGIN {scope=enterScope();} (e+=zakres | e+=expr | d+=decl)* {scope=leaveScope();}) -> blok(wyr={$e}, dekl={$d})
;

decl  :
//      ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
      ^(VAR i1=ID) {initializeVariable($ID.text, scope);} -> dek(n={$ID.text + scope.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st}) //tutaj nie używa się tych zmiennych z expr
        | ^(PODST i1=ID   e2=expr) {sc = isExist($ID.text, scope); if(sc==0){sc=scope;}} -> przypisz(name={$i1.text + sc.toString()}, val={$e2.st}) //tutaj text + numer
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID                       -> id(n={$ID.text + scope.toString()})
        | ^(IF e1=expr e2=expr e3=expr?){numer++;} -> if(e1={$e1.st},e2={$e2.st},e3={$e3.st},nr={numer.toString()})
    ;
    